from flask import render_template, redirect, url_for, flash, request, abort
from werkzeug.urls import url_parse
from flask_login import login_user, logout_user, current_user, login_required
from app import db
from app.auth import bp
from app.auth.forms import LoginForm, RegistrationForm
from app.models import User
import os
from dotenv import find_dotenv, load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))
@bp.before_request
def check_under_maintenance():
    if os.path.exists("maintenance"): # Check if a "maintenance" file exists (whatever it is empty or not)
        abort(503) # No need to worry about the current URL, redirection, etc

@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None:
            flash('Invalid username or password')
            return redirect(url_for('auth.login'))
        if user.active is True:
            if user is None or not user.check_password(form.password.data):
                flash('Invalid username or password')
                return redirect(url_for('auth.login'))
            login_user(user, remember=form.remember_me.data)
            user.logged_in = True
            db.session.commit()
            next_page = request.args.get('next')
            if not next_page or url_parse(next_page).netloc != '':
                next_page = url_for('main.index')
            return redirect(next_page)
        else:
            flash('User is inactive. Please contact an admin')
            return redirect(url_for('auth.login'))
    return render_template('auth/login.html', title='Login', form=form)

@bp.route('/logout')
def logout():
    user = User.query.filter_by(username=current_user.username).first()
    user.logged_in = False
    db.session.commit()
    logout_user()
    return redirect(url_for('main.index'))

@bp.route('/register', methods=['GET', 'POST'])
@login_required
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('auth.login'))
    return render_template('auth/register.html', title='Register', form=form)

@bp.route('dashboard', methods=['GET', 'POST'])
def dashboard():
    all_user = User.query.filter_by(logged_in=True).all()
    return render_template('auth/dashboard.html', title='Admin Dashboard', user=all_user)

# @bp.route('/all_user', methods=['GET','POST'])
# @login_required
# @roles_required('Admin')
# def all_users():
#     users = User.query.all()
#     return render_template('auth/all_users.html', title='All User', users=users)
#
#
# @bp.route('/edit_user', methods=['GET','POST'])
# @login_required
# @roles_required('Admin')
# def edit_user():
#     pass
