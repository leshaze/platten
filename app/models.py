from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db, login
from datetime import datetime

#define the User data-model
class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')
    logged_in = db.Column(db.Boolean(), server_default='0')
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), server_default='2')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

#Define the Role data-model
class Role(db.Model):
    __tablename__ = 'roles'
    id=db.Column(db.Integer, primary_key=True)
    name=db.Column(db.String(50), unique=True)
    user_role = db.relationship('User', backref='roles', lazy='dynamic')

class Label(db.Model):
    __tablename__ = 'labels'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, index=True)
    records = db.relationship('Record', backref='labels')

    def __repr__(self):
        return '<Label {}>'.format(self.name)

    def __getitem__(self, name):
        return self.name

class Country(db.Model):
    __tablename__ = 'countries'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, index=True)
    records = db.relationship('Record', backref='countries')

    def __repr__(self):
        return '<Country {}>'.format(self.name)

    def __getitem__(self, name):
        return self.name

class Artist(db.Model):
    __tablename__ = 'artists'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True, index=True)
    records = db.relationship('Record', backref='artists')

    def __repr__(self):
        return '<Artist {}>'.format(self.name)

    def __getitem__(self, name):
        return self.name


class Record(db.Model):
    __tablename__ = 'records'

    id = db.Column(db.Integer, primary_key=True)
    kind = db.Column(db.String(10))
    artist_id = db.Column(db.Integer, db.ForeignKey('artists.id'))
    title = db.Column(db.String(64), index=True)
    label_id = db.Column(db.Integer, db.ForeignKey('labels.id'))
    catalog_number = db.Column(db.String(64))
    matrix_number = db.Column(db.String(64))
    barcode = db.Column(db.String(64))
    country_of_origin_id = db.Column(db.Integer, db.ForeignKey('countries.id'))
    release_date = db.Column(db.String)
    reissue_date = db.Column(db.String)
    grading_media = db.Column(db.Integer)
    grading_cover = db.Column(db.Integer)
    current_price = db.Column(db.Numeric(4,2))
    buy_price = db.Column(db.Numeric(4,2))
    archive_number = db.Column(db.String(30), index = True)
    note = db.Column(db.Text(500))

    def __repr__(self):
        return '<Record {}>'.format(self.title)

    def __getitem__(self, title):
        return self.title

@login.user_loader
def load_user(id):
    return User.query.get(int(id))