from flask import render_template, flash, redirect, url_for, request, jsonify, abort
from app import db
from sqlalchemy import func
from app.main.forms import RecordsForm
from flask_login import login_required, current_user
from app.models import Record, Artist, Label, Country
from app.main import bp

import os
from dotenv import find_dotenv, load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))
DISCOGS_TOKEN = str(os.environ.get('DISCOGS_TOKEN'))
RECORDS_PER_PAGE = int(os.environ.get('RECORDS_PER_PAGE'))

@bp.before_request
def check_under_maintenance():
    if os.path.exists("maintenance"): # Check if a "maintenance" file exists (whatever it is empty or not)
        abort(503) # No need to worry about the current URL, redirection, etc

# def discogs(title, artists, catalog_number=None):
#
#     if catalog_number!=None:
#         print(catalog_number)
#         discogs = json.loads(requests.get('https://api.discogs.com/database/search?release_titel=' + \
#                                       title.replace(' ', '%20') + '&artist=' + artists.replace(' ', '%20') \
#                                       + '&catno=' + catalog_number.replace(' ', '%20') + '&token=' + \
#                                       DISCOGS_TOKEN + '').content)
#     else:
#         discogs = json.loads(requests.get('https://api.discogs.com/database/search?release_titel=' + \
#                                           title.replace(' ', '%20') + '&artist=' + artists.replace(' ', '%20') \
#                                           + '&token=' + \
#                                           DISCOGS_TOKEN + '').content)
#     if discogs['pagination']['items'] != 0:
#         #print(discogs)
#         lowest_price = 0
#         average_price = 0
#         i = 0
#
#         discogs_count = int(discogs['pagination']['items'])
#         #print(discogs_count)
#         if discogs_count >3:
#             discogs_count = 3
#         while i < discogs_count:
#             release_id = str(discogs['results'][i]['id'])
#             #print(release_id)
#             release = json.loads(requests.get('https://api.discogs.com/releases/' + release_id + '?EUR&token=' + DISCOGS_TOKEN + '').content)
#             #release = json.loads(requests.get('https://api.discogs.com/masters/' + release_id + '?token=' + DISCOGS_TOKEN + '').content)
#
#             #print(release)
#             if release['lowest_price'] == None:
#                 release['lowest_price'] = 0
#             #print(release['lowest_price'])
#             average_price += release['lowest_price']
#             if i == 0:
#                 lowest_price = release['lowest_price']
#             i += 1
#         average_price = average_price / discogs_count
#         return lowest_price, average_price

@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    if not current_user.is_anonymous:
        records = Record.query.all()
        records_count = db.session.query(func.count(Record.id)).scalar()
        artists_count = db.session.query(func.count(Artist.id)).scalar()
        labels_count = db.session.query(func.count(Label.id)).scalar()
        lp_count = db.session.query(func.count(Record.id)).filter_by(kind="LP").scalar()
        cd_count = db.session.query(func.count(Record.id)).filter_by(kind="CD").scalar()
        records_sum = db.session.query(func.sum(Record.current_price)).scalar()
        #print(artists_sum)
        return render_template('index.html', title ='Home', records=records, records_count=records_count, labels_count=labels_count, \
                               artists_count=artists_count, records_sum=records_sum, lp_count=lp_count, cd_count=cd_count)
        #return records_show_all()
    else:
        return render_template('index.html', title='Home')

@bp.route('/show_all', methods=['GET', 'POST'])
@login_required
def records_show_all():
    records = Record.query.join(Artist).order_by(Record.kind.desc(), Artist.name)

    return render_template('records_show_all.html', title='Show all records', records=records)


@bp.route('/add', methods=['GET', 'POST'])
@login_required
def records_add():
    form = RecordsForm()
    if form.validate_on_submit():
        artist = Artist.query.filter_by(name=form.artist.data).first()
        if not artist and form.artist.data != "":
            artist = Artist(name=form.artist.data)
            db.session.add(artist)
        label = Label.query.filter_by(name=form.label.data).first()
        if not label and form.label.data != "":
            label= Label(name=form.label.data)
            db.session.add(label)
        country = Country.query.filter_by(name=form.country_of_origin.data).first()
        if not country and form.country_of_origin.data != "":
            country = Country(name=form.country_of_origin.data)
            db.session.add(country)

        record = Record(kind=form.kind.data, title=form.title.data, catalog_number=form.catalog_number.data, matrix_number=form.matrix_number.data, barcode=form.barcode.data, \
                        release_date=form.release_date.data, reissue_date=form.reissue_date.data, grading_media=form.grading_media.data, grading_cover=form.grading_cover.data, \
                        current_price=form.current_price.data, buy_price=form.buy_price.data, archive_number=form.archive_number.data, note=form.note.data)
        if form.label.data:
            label.records.append(record)
        else:
            record.label_id = ""
        if form.artist.data:
            artist.records.append(record)
        if form.country_of_origin.data:
            country.records.append(record)
        else:
            record.country_of_origin_id = ""

        # db.session.add(record)
        db.session.commit()
        if form.kind.data == "CD":
            flash('CD ' + record.title + ' von ' + artist.name + ', hinzugefügt!')
        elif form.kind.data == "LP":
            flash('Platte ' + record.title + ' von ' + artist.name + ', hinzugefügt!')

        return redirect(url_for('main.records_add'))
    return render_template('records_add.html', title='Add', form=form)

@bp.route('/update/<int:id>', methods=['GET', 'POST'])
@login_required
def records_update(id):
    record = Record.query.get_or_404(id)
    if Artist.query.filter_by(id=record.artist_id).first():
        record.artist = Artist.query.filter_by(id=record.artist_id).first().name
    if Label.query.filter_by(id=record.label_id).first():
        record.label = Label.query.filter_by(id=record.label_id).first().name
    if Country.query.filter_by(id=record.country_of_origin_id).first():
        record.country_of_origin = Country.query.filter_by(id=record.country_of_origin_id).first().name
    form = RecordsForm(obj=record)
    if form.validate_on_submit():
        artist = Artist.query.filter_by(name=form.artist.data).first()
        if not artist and form.artist.data != "":
            artist = Artist(name=form.artist.data)
            db.session.add(artist)
        label = Label.query.filter_by(name=form.label.data).first()
        if not label and form.label.data != "":
            label= Label(name=form.label.data)
            db.session.add(label)
        country = Country.query.filter_by(name=form.country_of_origin.data).first()
        if not country and form.country_of_origin.data != "":
            country = Country(name=form.country_of_origin.data)
            db.session.add(country)
        record.kind = form.kind.data
        record.title = form.title.data
        record.catalog_number = form.catalog_number.data
        record.matrix_number = form.matrix_number.data
        record.barcode = form.barcode.data
        record.release_date = form.release_date.data
        record.reissue_date = form.reissue_date.data
        record.grading_media = form.grading_media.data
        record.grading_cover = form.grading_cover.data
        record.current_price = form.current_price.data
        record.buy_price = form.buy_price.data
        record.archive_number = form.archive_number.data
        record.note = form.note.data

        if form.label.data:
            label.records.append(record)
        else:
            record.label_id = ""
        if form.artist.data:
            artist.records.append(record)
        if form.country_of_origin.data:
            country.records.append(record)
        else:
            record.country_of_origin_id = ""

        # db.session.add(record)
        db.session.commit()

        if form.kind.data == "CD":
            flash('CD ' + record.title + ' von ' + artist.name + ', wurde geändert!')
        elif form.kind.data == "LP":
            flash('Platte ' + record.title + ' von ' + artist.name + ', wurde geändert!')

        return redirect(url_for('main.records_show_all'))
    return render_template('records_update.html', title='Update', form=form, action="Edit")

@bp.route('/delete/record/<int:id>', methods=['GET', 'POST'])
@login_required
def records_delete(id):
    print(id)
    record = Record.query.get_or_404(id)
    db.session.delete(record)
    db.session.commit()
    if record.kind == "CD":
        flash('CD ' + record.title + ' erfolgreich gelöscht!')
    elif record.kind == "LP":
        flash('Platte ' + record.title + ' erfolgreich gelöscht!')
    # redirect to the records page
    return redirect(url_for('main.records_show_all'))


@bp.route('/artist_details/<int:id>', methods=['GET', 'POST'])
@login_required
def artist_details(id):
    artist = Artist.query.get_or_404(id)
    artists_sum = db.session.query(func.sum(Record.current_price)).filter_by(artist_id=id).scalar()
    return render_template('artist_details.html', title='Details Artist', artist=artist, artists_sum=artists_sum)

@bp.route('/artists_all', methods=['GET', 'POST'])
@login_required
def artists_show_all():
    artists = db.session.query(Artist, func.count(Record.id).label('records_count')).select_from(Record).join(Record.artists).group_by(Artist)
    return render_template('artists_show_all.html', title='Show all artists', artists=artists)


@bp.route('/records_details/<int:id>', methods=['GET', 'POST'])
@login_required
def records_details(id):
    #id = request.args.get('id', 1, type=int)
    record = Record.query.get_or_404(id)
    # lowest_price, average_price = discogs(record.title, record.artists.name, record.catalog_number)
    # if lowest_price:
    #     return render_template('records_details.html', title='Details Record', record=record, lowest_price=lowest_price,
    #                            average_price=average_price)
    # else:
    return render_template('records_details.html', title='Details Record', record=record)

@bp.route('/label_details/<int:id>', methods=['GET', 'POST'])
@login_required
def label_details(id):
    #id = request.args.get('id', 1, type=int)
    label = Label.query.get_or_404(id)
    labels_sum = db.session.query(func.sum(Record.current_price)).filter_by(label_id=id).scalar()
    return render_template('label_details.html', title='Details Label', label=label, labels_sum=labels_sum)

@bp.route('/labels_all', methods=['GET', 'POST'])
@login_required
def labels_show_all():
    labels = db.session.query(Label, func.count(Record.id).label('records_count')).select_from(Record).join(
        Record.labels).group_by(Label)
    return render_template('labels_show_all.html', title='Show all artists', labels=labels)

@bp.route('/autocomplete', methods=['GET', 'POST'])
@login_required
def autocomplete():
    search=request.args.get('q')
    content= request.args.get('c')
    if content == "artist":
        query = Artist.query.filter(Artist.name.like('%' + str(search) + '%'))
        results = [at[1] for at in query.all()]
        return jsonify(matching_results=results)
    elif content == "label":
        query = Label.query.filter(Label.name.like('%' + str(search) + '%'))
        results = [at[1] for at in query.all()]
        return jsonify(matching_results=results)
    elif content == "country_of_origin":
        query = Country.query.filter(Country.name.like('%' + str(search) + '%'))
        results = [at[1] for at in query.all()]
        return jsonify(matching_results=results)
    # elif content == "search":
    #     query = Record.query.filter(Record.title.like('%' + str(search)+ '%') | Artist.name.like('%' + str(search)+ '%') | Label.name.like('%' + str(search)+ '%')).join(Artist).join(Label)
    #     results = [at[1] for at in query.all()]
    #     return jsonify(matching_results=results)
    else:
        return jsonify(matching_results="nothing")

