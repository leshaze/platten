from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, DateField, RadioField, DecimalField, TextAreaField, SelectField, validators
from wtforms.validators import ValidationError, DataRequired

class RecordsForm(FlaskForm):
    #             [100%   85%     70%     50%     35%     25%     15%     10%     5%]
    #     GER:    [M-     M--     VG++    VG+     VG      VG-     VG--    G+      G]
    #     UK:     [M-     EX+     EX      EX-     VG+     VG      VG-     G       G-]
    #     US:     [NM     NM      VG+     VG+     VG      VG      VG-     VG-     G]
    grading_choices = [('100', '100% - GER: M- / US: NM'), \
        ('85', '85% - GER: M-- / US: NM'), \
        ('70', '70% - GER: VG++ / US: VG+'), \
        ('50', '50% - GER: VG+ / US: VG+'), \
        ('35', '35% - GER: VG / US: VG'), \
        ('25', '25% - GER: VG- / US: VG'), \
        ('15', '15% - GER: VG-- / US: VG-'), \
        ('10', '10% - GER: G+ / US: VG-'), \
        ('5', '5% - GER G / US: G')]

    # country_choices = [('AR', 'Argentina'),
    #                    ('AU','Australia'),
    #                    ('AT', 'Austria'),
    #                    ('BE', 'Belgium'),
    #                    ('BR', 'Brazil'),
    #                    ('CA', 'Canada'),
    #                    ('DK', 'Denmark'),
    #                    ('FI', 'Finland'),
    #                    ('FR', 'France'),
    #                    ('DE', 'Germany'),
    #                    ('GR', 'Greece'),
    #                    ('HK', 'Hong Kong'),
    #                    ('IT', 'Italy'),
    #                    ('LI', 'Liechtenstein'),
    #                    ('MT', 'Malta'),
    #                    ('MX', 'Mexico'),
    #                    ('NL', 'Netherlands'),
    #                    ('NZ', 'New Zealand'),
    #                    ('PL', 'Poland'),
    #                    ('SE', 'Sweden'),
    #                    ('GB', 'United Kingdom'),
    #                    ('US', 'United States')]


    kind = RadioField('CD/LP', choices=[('CD', 'CD'), ('LP','LP')], default='LP', render_kw={"style":"list-style-type:none", "class": "list-inline"})
    artist = StringField('Artist', validators=[DataRequired()], id='autocomplete_artist', render_kw={"placeholder": "Künstler", "class": "form-control"})
    title = StringField('Titel', validators=[DataRequired()], render_kw={"placeholder": "Titel", "class": "form-control"})
    label = StringField('Label', id='autocomplete_label', render_kw={"placeholder": "Label", "class": "form-control"})
    catalog_number = StringField('Katalog-Nr.', render_kw={"placeholder": "Katalog-Nr", "class": "form-control"})
    matrix_number  = StringField('Matrix-Nr.', render_kw={"placeholder": "Matrix-Nr", "class": "form-control"})
    barcode = StringField('Barcode', render_kw={"placeholder": "Barcode", "class": "form-control"})
    country_of_origin = StringField('Herstellungsland', id='autocomplete_country', render_kw={"placeholder": "Herstellungsland", "class": "form-control"})
    release_date = StringField('Herstelldatum', render_kw={"placeholder": "Herstelldatum", "class": "form-control"})
    reissue_date = StringField('Reissue-Datum', render_kw={"placeholder": "Reissue-Datum", "class": "form-control"})
    grading_media = SelectField('Bewertung Media', choices=grading_choices, default='100', render_kw={"class": "form-control"})
    grading_cover = SelectField('Bewertung Cover', choices=grading_choices, default='100', render_kw={"class": "form-control"})
    current_price = DecimalField('Aktueller Preis', validators=(validators.Optional(),), render_kw={"placeholder": "Aktueller Preis", "class": "form-control"})
    buy_price = DecimalField('Kaufpreis',validators=(validators.Optional(),), render_kw={"placeholder": "Kaufpreis", "class": "form-control"})
    archive_number = StringField('Archive-Nr.', render_kw={"placeholder": "Archiv-Nr.", "class": "form-control"})
    note = TextAreaField('Bemerkung', render_kw={"placeholder": "Bemerkung", "class": "form-control"})
    submit = SubmitField('Submit')
